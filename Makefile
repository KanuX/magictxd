ifndef CXX
	CXX := clang++
endif

ifndef CXXFLAGS
	CXXFLAGS := --std=c++11 -g -Wall
endif

ESCAPE        :=  \033
RED           :=  $(ESCAPE)[31m
BOLD_RED      :=  $(ESCAPE)[1;31m
GREEN         :=  $(ESCAPE)[32m
BOLD_GREEN    :=  $(ESCAPE)[1;32m
YELLOW        :=  $(ESCAPE)[33m
BOLD_YELLOW   :=  $(ESCAPE)[1;33m
BLUE          :=  $(ESCAPE)[34m
BOLD_BLUE     :=  $(ESCAPE)[1;34m
PURPLE        :=  $(ESCAPE)[35m
BOLD_PURPLE   :=  $(ESCAPE)[1;35m
CYAN          :=  $(ESCAPE)[36m
BOLD_CYAN     :=  $(ESCAPE)[1;36m
RESET_COLOUR  :=  $(ESCAPE)[0m

PWD           :=  $(shell pwd)
OBJ_DIR       :=  $(PWD)/obj/
SRC_DIR       :=  $(PWD)/src/
BIN_DIR       :=  $(PWD)/bin/
DIRS          :=  obj/ obj/tools/ bin/
HEADERS       :=  -I./include/ -I./magic_api/
LIBRARIES     :=  

ifndef MINGW_CHOST
	LIBRARIES   +=
else
	LIBRARIES   +=
endif

_TARGETS      :=  magictxd

_SOURCES      :=  MagicExport.cpp \
                  aboutdialog.cpp \
                  createtxddlg.cpp \
                  embedded_resources.cpp \
                  expensivetasks.cpp \
                  exportallwindow.cpp \
                  friendlyicons.cpp \
                  guiserialization.cpp \
                  guiserialization.store.cpp \
                  helperruntime.cpp \
                  languages.common.cpp \
                  languages.cpp \
                  main.cpp \
                  mainwindow.actions.cpp \
                  mainwindow.asynclog.cpp \
                  mainwindow.cpp \
                  mainwindow.events.cpp \
                  mainwindow.loadinganim.cpp \
                  mainwindow.menu.cpp \
                  mainwindow.parallel.cpp \
                  mainwindow.rwplugins.cpp \
                  mainwindow.safety.cpp \
                  mainwindow.serialize.cpp \
                  mainwindow.themes.cpp \
                  mainwindow.txdprocessing.cpp \
                  massbuild.cpp \
                  massconvert.cpp \
                  massexport.cpp \
                  optionsdialog.cpp \
                  progresslogedit.cpp \
                  qtfilesystem.cpp \
                  qtutils.cpp \
                  renderpropwindow.cpp \
                  rwfswrap.cpp \
                  rwimageimporter.cpp \
                  rwversiondialog.cpp \
                  streamcompress.cpp \
                  streamcompress.lzo.cpp \
                  streamcompress.mh2z.cpp \
                  taskcompletionwindow.cpp \
                  texadddialog.cpp \
                  texformatextensions.cpp \
                  texnamewindow.cpp \
                  textureviewport.cpp \
                  tools/configtree.cpp \
                  tools/txdbuild.cpp \
                  tools/txdexport.cpp \
                  tools/txdgen.cpp \
                  txdlog.cpp

_OBJECTS      :=  MagicExport.o \
                  aboutdialog.o \
                  createtxddlg.o \
                  embedded_resources.o \
                  expensivetasks.o \
                  exportallwindow.o \
                  friendlyicons.o \
                  guiserialization.o \
                  guiserialization.store.o \
                  helperruntime.o \
                  languages.common.o \
                  languages.o \
                  main.o \
                  mainwindow.actions.o \
                  mainwindow.asynclog.o \
                  mainwindow.o \
                  mainwindow.events.o \
                  mainwindow.loadinganim.o \
                  mainwindow.menu.o \
                  mainwindow.parallel.o \
                  mainwindow.rwplugins.o \
                  mainwindow.safety.o \
                  mainwindow.serialize.o \
                  mainwindow.themes.o \
                  mainwindow.txdprocessing.o \
                  massbuild.o \
                  massconvert.o \
                  massexport.o \
                  optionsdialog.o \
                  progresslogedit.o \
                  qtfilesystem.o \
                  qtutils.o \
                  renderpropwindow.o \
                  rwfswrap.o \
                  rwimageimporter.o \
                  rwversiondialog.o \
                  streamcompress.o \
                  streamcompress.lzo.o \
                  streamcompress.mh2z.o \
                  taskcompletionwindow.o \
                  texadddialog.o \
                  texformatextensions.o \
                  texnamewindow.o \
                  textureviewport.o \
                  tools/configtree.o \
                  tools/txdbuild.o \
                  tools/txdexport.o \
                  tools/txdgen.o \
                  txdlog.o

TARGETS       :=  $(addprefix $(BIN_DIR)/, $(_TARGETS))
SOURCES       :=  $(addprefix $(SRC_DIR)/, $(_SOURCES))
OBJECTS       :=  $(addprefix $(OBJ_DIR)/, $(_OBJECTS))

.PHONY: all

all: directories $(TARGETS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@printf "[CXX] ""$(GREEN)""Building object '$<'""$(RESET_COLOUR)""\n"
	@$(CXX) $(CXXFLAGS) $(HEADERS) -c $< -o $@

$(TARGETS): $(OBJECTS)
	@printf "[BIN] ""$(BOLD_GREEN)""Linking binary '$@'""$(RESET_COLOUR)""\n"
	@$(CXX) $^ $(LIBRARIES) $(HEADERS) -o $@

directories: $(DIRS)

$(DIRS):
	@printf "[DIR] ""$(BLUE)""Directory ""$(BOLD_BLUE)""'$@'""$(RESET_COLOUR)$(BLUE)"" created""$(RESET_COLOUR).""\n"
	@mkdir -p $(PWD)/$@

clean:
	@rm -rv $(BIN_DIR) || true
	@rm -rv $(OBJ_DIR) || true

